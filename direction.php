<?php

enum Direction
{
    case Up;
    case Left;
    case Right;
    case Down;

    public static function fromString(string $direction) {
        switch ($direction) {
            case 'up':
                return Direction::Up;
            case 'left':
                return Direction::Left;
            case 'right':
                return Direction::Right;
            case 'down':
                return Direction::Down;
        }
    }
}

?>