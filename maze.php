<?php

include("direction.php");
include("position.php");

class Maze
{
    public int $size;
    public Position $player;
    public Position $exit;
    public array $walls;

    public function __construct()
    {
        $this->size = 5;
        $this->player = new Position(0, 0);
        $this->exit = new Position(4, 4);
        $this->walls = array(
            new Position(1, 0),
            new Position(1, 1),
            new Position(1, 2),
            new Position(1, 3),
            new Position(3, 0),
            new Position(3, 2),
            new Position(3, 3),
            new Position(3, 4)
        );
    }

    public function movePlayer(Direction $direction)
    {
        switch ($direction) {
            case Direction::Up:
                if ($this->player->row > 0 && !$this->isAnyWallForPosition($this->player->row - 1, $this->player->column)) {
                    $this->player->row--;
                }
                break;
            case Direction::Left:
                if ($this->player->column > 0 && !$this->isAnyWallForPosition($this->player->row, $this->player->column - 1)) {
                    $this->player->column--;
                }
                break;
            case Direction::Right:
                if ($this->player->column < $this->size - 1 && !$this->isAnyWallForPosition($this->player->row, $this->player->column + 1)) {
                    $this->player->column++;
                }
                break;
            case Direction::Down:
                if ($this->player->row < $this->size - 1 && !$this->isAnyWallForPosition($this->player->row + 1, $this->player->column)) {
                    $this->player->row++;
                }
                break;
        }
    }

    public function isAnyWallForPosition(int $row, int $column)
    {
        return count(array_filter($this->walls, function ($w) use ($row, $column) {
            return $w->match($row, $column);
        })) > 0;
    }

    public function isAnyWallForPosition2(int $row, int $column)
    {
        foreach ($this->walls as $wall) {
            if ($wall->match($row, $column)) {
                return true;
            }
        }
        return false;
    }
}
