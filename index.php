<?php
include("maze.php");

session_start();

$maze = new Maze;

if (!isset($_GET['reset']) && isset($_SESSION['maze'])) {
    $maze = $_SESSION['maze'];
}

if (isset($_GET['direction'])) {
    $direction = Direction::fromString($_GET['direction']);
    if (!is_null($direction)) {
        $maze->movePlayer($direction);
    }
}

$_SESSION['maze'] = $maze;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Maze</title>
    <link rel="stylesheet" href="/css/site.css" />
</head>

<body>
    <main>
        <div class="maze" style="--size: <?php echo $maze->size ?>">
            <?php for ($row = 0; $row < $maze->size; $row++) { ?>
                <div class="row">
                    <?php for ($column = 0; $column < $maze->size; $column++) { ?>
                        <div class="column">
                            <?php if ($maze->isAnyWallForPosition($row, $column)) { ?>
                                <img class="player" src="/image/wall.png">
                            <?php } ?>
                            <?php if ($maze->exit->match($row, $column)) { ?>
                                <img class="player" src="/image/exit.png">
                            <?php } ?>
                            <?php if ($maze->player->match($row, $column)) { ?>
                                <img class="player" src="/image/player.png">
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <div class="actions">
            <div class="move-pad">
                <div class="direction">
                    <a class="button" href="/?direction=up">⇧</a>
                </div>
                <div class="horizontal">
                    <div class="direction">
                        <a class="button" href="/?direction=left">⇦</a>
                    </div>
                    <div class="direction empty"></div>
                    <div class="direction">
                        <a class="button" href="/?direction=right">⇨</a>
                    </div>
                </div>
                <div class="direction">
                    <a class="button" href="/?direction=down">⇩</a>
                </div>
            </div>
            <div class="game-actions">
                <a class="button reset" href="/?reset">Réinitialiser</a>
            </div>
        </div>
    </main>
    <script>
        window.onkeydown = (event) => {
            switch (event.keyCode) {
                case 37:
                    location.href = '/?direction=left';
                    break;
                case 38:
                    location.href = '/?direction=up';
                    break;
                case 39:
                    location.href = '/?direction=right';
                    break;
                case 40:
                    location.href = '/?direction=down';
                    break;
            }
        }
    </script>

    <script src="/js/site.js"></script>
</body>

</html>