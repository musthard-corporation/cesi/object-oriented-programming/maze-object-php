<?php

class Position
{
    public int $row;
    public int $column;

    public function __construct(int $row, int $column)
    {
        $this->row = $row;
        $this->column = $column;
    }

    public function match(int $row, int $column)
    {
        return $this->row == $row && $this->column == $column;
    }

    public function matchPosition(Position $position)
    {
        return $this->row == $position->row && $this->column == $position->column;
    }
}
